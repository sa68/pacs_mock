import os
import time

from pydicom import dcmread
from pydicom.dataset import Dataset

from pynetdicom import AE, StoragePresentationContexts, evt
from pynetdicom.sop_class import PatientRootQueryRetrieveInformationModelGet, StudyRootQueryRetrieveInformationModelGet, \
    PatientRootQueryRetrieveInformationModelFind, StudyRootQueryRetrieveInformationModelFind, \
    PatientStudyOnlyQueryRetrieveInformationModelFind, PatientRootQueryRetrieveInformationModelMove, \
    StudyRootQueryRetrieveInformationModelMove, PatientStudyOnlyQueryRetrieveInformationModelMove, \
    CompositeInstanceRootRetrieveMove

#fileDir = '/home/sa/Documents/LIDC-IDRI/LIDC-IDRI-0884/1.3.6.1.4.1.14519.5.2.1.6279.6001.219603183011909533511019563626/1.3.6.1.4.1.14519.5.2.1.6279.6001.892375496445736188832556446335'
fileDir = 'C:/Users/Priem/Documents/dicom/other/LIDC-IDRI-0001/1.3.6.1.4.1.14519.5.2.1.6279.6001.298806137288633453246975630178/1.3.6.1.4.1.14519.5.2.1.6279.6001.179049373636438705059720603192'

known_aet_dict = {"CI_GW_STAGE_MOVE": ("10.10.101.20", 14006),
                  "SA2": ("10.10.101.52", 8002)}

def get_matching(ds, instances):
    """Use for GET and Move query. Find matching instances and ds"""
    matching = []
    if ds.QueryRetrieveLevel == 'PATIENT':
        if 'PatientID' in ds:
            matching = [
                inst for inst in instances if inst.PatientID == ds.PatientID
            ]
    if ds.QueryRetrieveLevel == 'STUDY':
        if 'StudyInstanceUID' in ds:
            matching = [
                inst for inst in instances if inst.StudyInstanceUID == ds.StudyInstanceUID
            ]

    if ds.QueryRetrieveLevel == 'SERIES':
        if 'SERIESInstanceUID' in ds:
            matching = [
                inst for inst in instances if inst.SERIESInstanceUID == ds.SERIESInstanceUID
            ]

    if ds.QueryRetrieveLevel == 'IMAGE':
        if 'SOPInstanceUID' in ds:
            matching = [
                inst for inst in instances if inst.SOPInstanceUID == ds.SOPInstanceUID
            ]

    return matching


def handle_find(event, fdir=fileDir):
    """Handle a C-FIND request event."""
    ds = event.identifier
    # print(ds)
    # Import stored SOP Instances
    instances = []
    for fpath in os.listdir(fdir):
        instances.append(dcmread(os.path.join(fdir, fpath)))
    if 'QueryRetrieveLevel' not in ds:
        # Failure
        yield 0xC000, None
        return
    filters = ds.dir()

    matching = instances
    for x in filters:
        if x not in ('QueryRetrieveLevel', 'StudyDate'):
            if ds[x].value not in ['*', '', '?']:
                matching = list(filter(lambda a: a[x].value == ds[x].value, matching))

    for instance in matching:
        identifier = Dataset()
        identifier.QueryRetrieveLevel = ds.QueryRetrieveLevel
        # identifier.PatientName = ds.PatientName
        for x in filters:
            if x != 'QueryRetrieveLevel':
                if ds[x].value in ['*', '', '?']:
                    identifier[x] = instance[x]
        print(identifier)
        # Pending
        yield (0xFF00, identifier)


# Implement the handler for evt.EVT_C_GET
def handle_get(event, fdir=fileDir):
    """Handle a C-GET request event."""
    ds = event.identifier
    if 'QueryRetrieveLevel' not in ds:
        # Failure
        yield 0xC000, None
        return

    # Import stored SOP Instances
    instances = []
    for fpath in os.listdir(fdir):
        try:
            instances.append(dcmread(os.path.join(fdir, fpath)))
        except:
            print(fpath, "is not dicom")

    matching = get_matching(ds, instances)

    # Yield the total number of C-STORE sub-operations required
    yield len(instances)

    # Yield the matching instances
    for instance in matching:
        # Check if C-CANCEL has been received
        if event.is_cancelled:
            yield (0xFE00, None)
            return

        # Pending
        yield (0xFF00, instance)



# Implement the evt.EVT_C_MOVE handler
def handle_move(event, fdir=fileDir):
    """Handle a C-MOVE request event."""
    ds = event.identifier

    if 'QueryRetrieveLevel' not in ds:
        # Failure
        yield 0xC000, None
        return

    # get_known_aet() is here to represent a user-implemented method of
    #   getting known AEs, for this example it returns a dict with the
    #   AE titles as keys


    try:
        a = event.move_destination.decode("utf-8")
        b = a.split()
        b = ''.join(b)
        (addr, port) = (known_aet_dict[b])
    except KeyError:
        # Unknown destination AE
        yield (None, None)
        return

    # Yield the IP address and listen port of the destination AE
    yield (addr, port)

    # Import stored SOP Instances
    instances = []
    for fpath in os.listdir(fdir):
        try:
            instances.append(dcmread(os.path.join(fdir, fpath)))
        except:
            print(fpath, "is not dicom")

    matching = get_matching(ds, instances)

    yield len(matching)

    # Yield the matching instances
    for instance in matching:
        # Check if C-CANCEL has been received
        if event.is_cancelled:
            yield (0xFE00, None)
            return

        # Pending
        yield (0xFF00, instance)


handlers = [(evt.EVT_C_GET, handle_get), (evt.EVT_C_FIND, handle_find), (evt.EVT_C_MOVE, handle_move)]

# Create application entity
ae = AE()

# Add the requested presentation contexts (Storage SCU)
ae.requested_contexts = StoragePresentationContexts
# Add a supported presentation context (QR Move SCP)
ae.add_supported_context(PatientRootQueryRetrieveInformationModelFind)
ae.add_supported_context(StudyRootQueryRetrieveInformationModelFind)
ae.add_supported_context(PatientStudyOnlyQueryRetrieveInformationModelFind)
ae.add_supported_context(PatientRootQueryRetrieveInformationModelGet)
ae.add_supported_context(StudyRootQueryRetrieveInformationModelGet)
ae.add_supported_context(PatientRootQueryRetrieveInformationModelMove)
ae.add_supported_context(StudyRootQueryRetrieveInformationModelMove)
ae.add_supported_context(PatientStudyOnlyQueryRetrieveInformationModelMove)
ae.add_supported_context(CompositeInstanceRootRetrieveMove)

for cx in ae.supported_contexts:
    cx.scp_role = True
    cx.scu_role = False

# Start listening for incoming association requests
ae.start_server(('10.43.0.6', 11114), evt_handlers=handlers, ae_title=b'TST')